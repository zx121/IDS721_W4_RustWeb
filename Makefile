format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

run:
	cargo run

install:
	cargo clean &&\
		cargo build -j 1

build:
	docker build -t rust_actix_drink_vending_machine .

dockerrun:
	#docker run -it --rm -p 8080:8080 rust_actix_drink_vending_machine
	docker run -dp 8080:8080 rust_actix_drink_vending_machine

all: format lint test run
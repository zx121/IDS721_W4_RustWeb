# Containerize a Rust Actix Web Service

## Requirements
- Containerize simple Rust Actix web app
- Build Docker image
- Run container locally

## Preparation
1. Set up Rust development environment

You can follow the guidance [here](https://www.rust-lang.org/learn/get-started) to prepare for the environment. Make sure you have these key components: **Rustup** (the Rust installer and version management tool) and **Cargo** ([the Rust build tool and package manager](https://doc.rust-lang.org/cargo/getting-started/first-steps.html))

```bash
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# For MacOS and Linux
curl https://sh.rustup.rs -sSf | sh

# Configure the current shell
source $HOME/.cargo/env

# Start a new package with Cargo. Cargo defaults to --bin to make a binary program. To make a library, we would pass --lib, instead.
cargo new rust_actix_drink_vending_machine
```

An easier way to prepare for it is using the github codespace with a `.devcontainer` folder. Include the `Dockerfile`and `devcontainer.json` in it. However, you can not try to build a docker image on your own within the codespace run by `.devcontainer`. Maybe github codespace do not support this feature :(
```Dockerfile
FROM mcr.microsoft.com/devcontainers/rust:0-1-bullseye

# Include lld linker to improve build times either by using environment variable
# RUSTFLAGS="-C link-arg=-fuse-ld=lld" or with Cargo's configuration file (i.e see .cargo/config.toml).
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
   && apt-get -y install clang lld \
   && apt-get autoremove -y && apt-get clean -y
```

2. Add dependencies

You may edit the `Cargo.toml` as the following content.
```toml
[package]
name = "rust_actix_drink_vending_machine"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
axum = "0.7.4"
tokio = { version = "1.36.0", features = ["full"] }
tower = "0.4.13"
serde = { version = "1.0.196", features = ["derive"] }
serde_json = "1.0.113"
actix-web = "4.5.1"
``` 

3. Make your own microservices
- Create `lib.rs`file to hold your data processing functions and tests.
- Create `main.rs` to implement the HttpServer router for different services.

Here as for my project, I built a auto drink vending machine. You can check the brand of drinks and their price with the URI `/drinks`, which shows the json format content like:
```json
[
    {
        "name": "Coke",
        "price": 150
    },
    {
        "name": "Pepsi",
        "price": 150
    },
    {
        "name": "Water",
        "price": 100
    }
]
```
Then, you can buy the drink you wnat with the URL request like `/purchase?drink=Coke&amount=1`. It will shows that "Insufficient amount. Coke costs $150". If you set amount=150, you will see "Purchased Coke for $150". You can get the detailed information at the next part in README.


## Run the project
1. Navigate to your project directory in the terminal and run the project with Cargo. The project will be hold on the local machine with specific port(127.0.0.1:8080).
```bash
cargo run
```
2. Test accessibility
You can visit the url or use command tool like curl. Also `Postman` is a good way. 
```bash
curl http://127.0.0.1:8080/
```
- root endpoint
![Alt text](image-1.png)

- visit endpoint /drinks
![Alt text](image.png)

- visit endpoint /purchase
![Alt text](image-2.png)

- error handling
![Alt text](image-3.png)
![Alt text](image-4.png)

## Build with Docker
* Build the Docker Image. Navigate to your project current folder and run: `docker build -t <YOUR-IMAGE-NAME> .`.
* Run the Container. After the image has been built, you can run it as a container with command `docker run -d -p 8080:8080 <YOUR-IMAGE-NAME>`. It runs the container and maps port 8080 inside the container to port 8080 on your host machine, allowing you to access the web application via `http://localhost:8080`. 
```bash
# Build the Docker image
docker build -t rust_actix_drink_vending_machine .

# Run the container in detached mode (background) mapping port 8080
docker run -dp 8080:8080 rust_actix_drink_vending_machine

# List running containers to find the container ID
docker ps
```

![Alt text](image-5.png)
![Alt text](image-6.png)

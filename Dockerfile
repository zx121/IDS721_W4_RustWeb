# Build stage
FROM rust:1.68 as builder
WORKDIR /app
COPY . .
RUN cargo build --release

FROM debian:bookworm-slim
COPY --from=builder /app/target/release/rust_actix_drink_vending_machine /app/

# Use non-root user
# USER nonroot:nonroot
USER root
# Set up app directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose port
EXPOSE 8080

# Run app
ENTRYPOINT ["/app/rust_actix_drink_vending_machine"]
